package models;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
// @Table(name = "Companies")
public class Company extends GenericModel {

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long companyId;

    @Column(name = "VAT")
    public String companyVAT;

    @Column(name = "name")
    public String companyName;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    public Address companyAddress;

    @OneToMany(mappedBy= "owningCompany",
               orphanRemoval = true)
    public Collection<Employee> employees;
    
}