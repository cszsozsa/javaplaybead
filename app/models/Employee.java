package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
// @Table(name = "Employees")
public class Employee extends GenericModel {

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long employeeId;

    @Column(name = "first_name")
    public String employeeFirstName;

    @Column(name = "last_name")
    public String employeeLastName;

    @Column(name = "id_number")
    public String employeeIdNumber;

    @Column(name = "salary")
    public Integer employeeSalary;

    @Column(name = "birth_date")
    public Date employeeBirthDate;

    @Column(name = "role")
    public String employeeRole;

    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    public Company owningCompany;
}