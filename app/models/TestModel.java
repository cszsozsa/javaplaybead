package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "test")
public class TestModel extends GenericModel {

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long testId;

    @Column(name = "text")
    public String testText;
}