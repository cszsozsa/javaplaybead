package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
// @Table(name = "Addresses")
public class Address extends GenericModel {

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long addressId;

    @Column(name = "postcode")
    public Integer addressPostcode;

    @Column(name = "city")
    public String addressCity;

    @Column(name = "number")
    public Integer addressNumber;

    @Column(name = "street")
    public String addressStreet;

    @OneToOne(mappedBy = "companyAddress")
    public Company owningCompany;

}