package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import models.Address;
import models.Company;
import models.TestModel;
import play.data.validation.Required;
import play.mvc.Controller;

public class CompanyCreateController extends Controller {

    public static void createCompanyForm(){
        render("@Application.Company.createCompany");
    }

    public static void createCompany( @Required(message = "A név kötelező")
                                      String companyName,
                                      @Required(message = "Az irányítószám kötelező!")
                                      String addressPostcode,
                                      @Required(message = "A város kötelező!")
                                      String addressCity,
                                      @Required(message = "Az utca kötelező!")
                                      String addressStreet,
                                      @Required(message = "A házszám kötelező!")
                                      String addressNumber,
                                      @Required(message = "Az adószám kötelező!")
                                      String companyVAT
                                      ){
        Integer postcode = null;
        Integer number = null;

        // TODO validálások                                            

        Company company = Company.find(" companyName = ? ", companyName).first();

        // Cégnév validálás
        if(company != null){
            validation.addError("companyName", "Ilyen nevű cég már létezik!");
        }

        // Postcode validálás
        if (!isInteger(addressPostcode)) {
			validation.addError("addressPostcode", "Az irányítószám nem lehet szöveges adat!");
		} else {
            postcode = Integer.valueOf(addressPostcode);
            
			if (postcode >= 10000 || postcode < 1000) {
				validation.addError("addressPostcode", 
						"Az irányítószám 1000 és 10000 között kell legyen");
			}
		}

        if(!isInteger(addressNumber)){
            validation.addError("addressNumber", "A házszám nem lehet szöveg!");            
        } else {
            number = Integer.valueOf(addressNumber);
        }

        // Minden ok?
        if (validation.hasErrors()) {
            params.flash();
            CompanyController.GetCompanies(null);
        } else {
            company = new Company();
            Address address = new Address();

            company.companyName = companyName;
            company.companyVAT = companyVAT;
            
            address.addressPostcode = postcode;
            address.addressCity = addressCity;
            address.addressNumber = number;
            address.addressStreet = addressStreet;

            company.companyAddress = address;

            address.save();
            company.save();

            CompanyController.GetCompanies(null);
        }
    }

    private static boolean isInteger(String string) {
        try {
            Integer integer = Integer.valueOf(string);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}