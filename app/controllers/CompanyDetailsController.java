package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import models.Address;
import models.Company;
import models.TestModel;
import play.data.validation.Required;
import play.mvc.Controller;

public class CompanyDetailsController extends Controller {
    
    public static void viewCompany( @Required(message = "Azonosító szükséges!")
                                       Long companyId){
                                            
        Company company = null;
        
        if(companyId != null){
            company = Company.findById(companyId);
        }

        if(company == null){
            CompanyController.GetCompanies(null);
        } else {
            renderArgs.put("company", company);
            render("@Application.Company.viewCompany");
        }        
    }
}