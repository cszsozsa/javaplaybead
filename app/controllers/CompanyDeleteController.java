package controllers;

import models.Company;
import play.mvc.Controller;

public class CompanyDeleteController extends Controller{

    public static void deleteCompany(Long companyId){
        Company company = Company.findById(companyId);

        if (company != null) {
            // Az entitásban be van állítva, így törli a childjait is.
            company.delete();
            CompanyController.GetCompanies(null);
        } else {
            flash.put("errorMessage", "Nem létezik ilyen cég!");
            CompanyController.GetCompanies(null);
        }
    }

}