package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import models.Employee;
import models.Company;
import play.mvc.Controller;

public class CompanyController extends Controller {
    
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private static final Logger LOGGER = Logger.getLogger(TestController.class);

    public static void GetCompanies(Long companyId){

        List<Company> companies = Company.findAll();
        
        renderArgs.put("companies", Arrays.asList(companies));
        //TODO companies csak a szükséges értékeket adja vissza, amit tud számoljon ki.(alk szám, pénz)
        
        render("@Application.index");
    }
}