package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import models.TestModel;
import play.mvc.Controller;

public class TestController extends Controller {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    // private static final Logger LOGGER = Logger.getLogger(TestController.class);

    public static void dbtest(String input1, String input2) {

        // input1 = "HelloKaaaa";
        // input2 = " asddasdfasdf";
        String concatenatedString = input1.concat(input2);
        renderArgs.put("myString", concatenatedString);
        renderArgs.put("dateToOutput", DATE_FORMAT.format(new Date()));

        render("@Application.dbtest");
    }

    public static void Tests(Long testId) {
        List<TestModel> tests = TestModel.findAll();

        if (testId == null) {
            for (TestModel test : tests) {
                renderArgs.put("test" + test.testId, test.testText);
            }
            renderArgs.put("tests", tests);
        } else {
            TestModel testsToShow = TestModel.findById(testId);
            if (testsToShow != null) {
                renderArgs.put("tests", Arrays.asList(testsToShow));
            } else {
                // Nincs találat.
            }
        }

        renderArgs.put("test", testId);
        render("@Application.dbtest");
    }

    public void name() {

    }
}