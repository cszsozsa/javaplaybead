package controllers;

import java.util.Date;
import java.util.List;

import models.Company;
import models.Employee;
import play.data.validation.Required;
import play.db.jpa.JPABase;
import play.mvc.Before;
import play.mvc.Controller;

public class EmployeeCreateController extends Controller {

    @Before
    public static void prepareEmployeeCreatePage(Long companyId) {

        List<Company> companies = Company.findAll();
        if (companyId != null) {
            Company company = Company.findById(companyId);    
            renderArgs.put("company", company);
        }
        renderArgs.put("companies", companies);
    }

    public static void createEmployeeForm(){
        List<Company> companies = (List<Company>) renderArgs.get("companies");
        if (companies.size() == 0) {
            flash.put("errorMessage","Nincsenek cégek!");
            CompanyController.GetCompanies(null);
        } else {
            render("@Application.Employee.createEmployee");
        }
    }

    public static void createEmployee(  @Required(message = "Az id kötelező!") Long companyId,
                                        @Required(message = "Az alkalmazott keresztneve kötelező!") String employeeFirstName,
                                        @Required(message = "Az alkalmazott vezetékneve kötelező!") String employeeLastName,
                                        String employeeIdNumber,
                                        Date employeeBirthDate,
                                        Integer employeeSalary,
                                        String employeeRole){
        if (validation.hasErrors()) {
            params.flash();
            CompanyController.GetCompanies(null);
        } else {
            Employee employee = new Employee();
            employee.owningCompany = Company.findById(params.get("companyId")); // Sajnos ezt nem tudtam átvenni
            employee.employeeFirstName = employeeFirstName;
            employee.employeeLastName = employeeLastName;
            employee.employeeIdNumber = employeeIdNumber;
            employee.employeeBirthDate = employeeBirthDate;
            employee.employeeSalary = employeeSalary;
            employee.employeeRole = employeeRole;                                            

            employee.save();
        }
        CompanyController.GetCompanies(null);
    }
}